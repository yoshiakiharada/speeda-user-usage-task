(ns dev
  (:refer-clojure :exclude [test])
  (:require [clojure.repl :refer :all]
            [fipp.edn :refer [pprint]]
            [clojure.tools.namespace.repl :refer [refresh]]
            [clojure.java.io :as io]
            [duct.core :as duct]
            [duct.core.repl :as duct-repl]
            [eftest.runner :as eftest]
            [integrant.core :as ig]
            [integrant.repl :refer [clear halt go init prep reset]]
            [integrant.repl.state :refer [config system]]
            [speeda-user-usage-task.log-db.boundary.usage-store :as logdb]
            [speeda-user-usage-task.boundary.usage-store :as su]))

(duct/load-hierarchy)

(defn read-config []
  (duct/read-config (io/resource "speeda_user_usage_task/config.edn")))

(defn test []
  (eftest/run-tests (eftest/find-tests "test")))

(def profiles
  [:duct.profile/dev :duct.profile/local])

(clojure.tools.namespace.repl/set-refresh-dirs "dev/src" "src" "test")

(when (io/resource "local.clj")
  (load "local"))

(integrant.repl/set-prep! #(duct/prep-config (read-config) profiles))

(defn get-usages
  [from to]
  (let [store (:speeda-user-usage-task.log-db.boundary/usage-store system)
        db-spec (:db-spec store)] 
    (logdb/get-usages (logdb/->LogUsageDb db-spec) from to)))

(defn get-last-store-date
  []
  (let [store (:speeda-user-usage-task.boundary/usage-store system)
        db-spec (:db-spec store)]
   (su/get-last-store-date (su/->SpeedaUserUsageDb db-spec))))
