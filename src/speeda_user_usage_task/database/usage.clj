(ns speeda-user-usage-task.database.usage
  (:require [next.jdbc :as jdbc]
            [honeysql.core :as sql]
            [honeysql.helpers :refer [select from where order-by limit] :as helpers]))

(defn insert-usages
  [conn usages])

(defn get-last-store-date
  [conn]
  (let [select-sql (-> (select :usage_date)
                        (from :usage_per_day)
                        (order-by [:usage_date :desc])
                        (limit 1))]
    (jdbc/execute! conn (sql/format select-sql))))
