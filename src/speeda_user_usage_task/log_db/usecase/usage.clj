(ns speeda-user-usage-task.log-db.usecase.usage
  (:require [speeda-user-usage-task.log-db.boundary.usage-store :as us]
            [java-time :as t]))

(defn get-previous-date
  [date]
  (t/minus date (t/days 1)))

(defn get-next-date
  [date]
  (t/plus (t/local-date) (t/days 1)))

(defn get-usages
  [{:keys [usage-store]} from to]
  (let [from-time (or from (t/minus (t/local-date) (t/month 1)))
        to-time (or to (get-previous-date (t/local-date)))]
    (us/get-usages usage-store from-time to-time)))
