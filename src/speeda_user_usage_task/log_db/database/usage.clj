(ns speeda-user-usage-task.log-db.database.usage
  (:require [next.jdbc :as jdbc]
            [honeysql.core :as sql]
            [honeysql.helpers :refer [select from where] :as helpers]))

(defn get-usages
  [conn from-date to-date]
  (let [select-sql (-> (select :*)
                       (from :usage_per_day)
                       (where [:>= :usage_date from-date] [:<= :usage_date to-date])
                       (sql/format))]
    (jdbc/execute! conn select-sql)))

