(ns speeda-user-usage-task.log-db.boundary.usage-store
  (:require [speeda-user-usage-task.log-db.database.usage :as db]
            [next.jdbc :as jdbc]
            [integrant.core :as ig]
            [java-time :as t]))

(defprotocol UsageStore
  (get-usages [self from to]))

(defrecord LogUsageDb
    [db-spec]
  UsageStore
  (get-usages [self from to]
    (let [conn (jdbc/get-datasource db-spec)]
      (db/get-usages conn (t/sql-date from) (t/sql-date to)))))

(defmethod ig/init-key :speeda-user-usage-task.log-db.boundary/usage-store
  [_ {:keys [db-spec] :as deps}]
  (->LogUsageDb db-spec))
