(ns speeda-user-usage-task.boundary.usage-store
  (:require [speeda-user-usage-task.database.usage :as db]
            [integrant.core :as ig]
            [next.jdbc :as jdbc]))

(defprotocol UsageStore
  (store [self usages] "引数で指定したusagesを保存する")
  (get-last-store-date [self] "最後にusageを保存した日を返す"))

(defrecord SpeedaUserUsageDb
    [db-spec]
  UsageStore
  (store [self usages]
    (let [conn (jdbc/get-datasource db-spec)]
      (db/insert-usages conn usages)))

  (get-last-store-date [self]
    (let [conn (jdbc/get-datasource db-spec)]
      (db/get-last-store-date conn))))

(defmethod ig/init-key :speeda-user-usage-task.boundary/usage-store
  [_ {:keys [db-spec] :as deps}]
  (->SpeedaUserUsageDb db-spec))
