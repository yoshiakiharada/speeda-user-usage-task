(ns speeda-user-usage-task.main
  (:gen-class)
  (:require [duct.core :as duct]
            [next.jdbc :as jdbc]
            [honeysql.core :as sql]
            [honeysql.helpers :refer [select from where] :as helpers]
            [java-time :as t]
            [integrant.core :as ig]))

(duct/load-hierarchy)

(def log-db {:dbtype "mysql"
             :dbname "log"
             :user   "system"
             :password   "system"
             :host   "127.0.0.1"
             :port   3306})

(def speeda-db {:dbtype "mysql"
                :dbname "user"
                :user   "developer"
                :password   "developer"
                :host   "speeda-db-user"
                :port   3306})

(defprotocol Runner
  (run [self opts]))

(defn get-db-con
  [db]
  (jdbc/get-datasource db))

(defn -main [& args]
  (let [keys     (or (duct/parse-keys args) [:duct/daemon])
        profiles [:duct.profile/prod]]
    (-> (duct/resource "speeda_user_usage_task/config.edn")
        (duct/read-config)
        (duct/exec-config profiles keys))))

(defmethod ig/init-key :speeda-user-usage-task.main/runner
  [_ {:keys [log-db speeda-user-db]  :as deps}]
  (reify Runner
    (run [self {:keys [from-time to-time log-db speeda-user-db] :as opts}]
      (print log-db)
      (print speeda-user-db))))
